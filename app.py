import json
import requests
import shutil
import datetime
from flask import Flask
from flask import render_template, url_for
app = Flask(__name__)

def download(url):
    '''
    download movie's cover from url and change img path
    '''
    response = requests.get(url, stream=True)
    f_name = url[-15:]
    with open('static/'+f_name, 'wb') as f:
        response.decode_content = True
        shutil.copyfileobj(response.raw, f)
        del response

    return f_name

# list last x days from today
def last_x_days(x):
    last_x_days = []
    today = datetime.date.today()
    for i in range(x):
        last_x_days.append((today - datetime.timedelta(days=i+1)).strftime("%Y-%m-%d"))
    return last_x_days


def top5():
    '''
    get top5 movies from movies.json
    '''
    # load movies.json file
    with open('movies.json', 'r') as f:
        movies = json.load(f)
    # print(len(movies))

    # filter movies by rating people greater than 10000 and release date in 90 days
    f = filter(lambda x: x['rating_people']>=10000 and x['date'] in last_x_days(90), movies)

    # sort movies by rate number
    s = sorted(f, key=lambda x: float(x['rate']), reverse=True)
    # pprint(outcome)


    outcome = []
    # download top 5 movie's cover from url and change img path
    for i in s[:10]:
        i['cover'] = download(i['cover'])
        outcome.append(i)

    # return top5 movies
    return outcome

@app.route('/')
def show_index():
    '''
    show top5 movie' info page
    '''
    return render_template('home.html', top5=top5())

@app.route('/team/')
def team():
    '''
    show team member info
    '''
    return render_template('team.html')

if __name__ == '__main__':
    app.run()